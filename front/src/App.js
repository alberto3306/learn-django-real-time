import React, { Component } from 'react'
import './App.css'
import Backend from './Backend'

class App extends Component {

    render() {

        const backendHost = window.location.hostname + ':8000'

        return (
            <div className="App">
                <Backend host={backendHost} />
            </div>
        )
    }
}

export default App;
