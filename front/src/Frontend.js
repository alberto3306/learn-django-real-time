import React from 'react';
import TaskKinds from './TaskKinds';
import MessageList from './MessageList';
import OnlineStatus from './OnlineStatus';



const frontend = (props) => {

    return (
        <div>
            <OnlineStatus connected={props.connected} />
            <TaskKinds
                kinds={props.kinds}
                createTaskCallback={props.createTaskCallback}
            />
            <MessageList messages={props.messages} />
        </div>
    )
}

export default frontend