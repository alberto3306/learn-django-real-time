import { Component } from 'react'

class Socket extends Component {

    state = {
        socket: null,
    }

    // REACT METHODS

    render() {
        return null
    }

    componentDidMount() {
        this.connect()
    }

    connect() {
        if (!this.isConnected()) {
            console.log('Connecting...')
            this.setState({
                socket: this.createNewSocket()
            })
        }
    }

    isConnected() {
        return (this.state.socket !== null && this.state.socket.readyState !== WebSocket.CLOSED)
    }

    createNewSocket() {
        const socket = new WebSocket(this.props.url)
        socket.onmessage = (e) => this.handleSocketMessage(e)
        socket.onopen = (e) => this.handleSocketOpen(e)
        socket.onclose = (e) => this.handleSocketClose(e)
        return socket
    }

    handleSocketMessage(e) {
        console.log('Socket message received.')
        this.props.onMessage(e.data)
    }

    handleSocketOpen(e) {
        console.log('Socket opened.')
        this.props.onConnect()
    }

    handleSocketClose(e) {
        console.log('Socket closed.')
        this.props.onDisconnect()
        this.connect()
    }
}

export default Socket