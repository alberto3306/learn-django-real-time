import React from 'react'
import CreateTask from './CreateTask'

const taskKinds = (props) => {
    return (
        <div className="row">
            <div className="col-sm-12">
                <table className="table">
                    <tbody>
                        {
                            props.kinds.map(k => (
                                <tr key={k.name}>
                                    <th>{k.name}</th>
                                    <td>{k.running_since}</td>
                                    <td><CreateTask callback={() => props.createTaskCallback(k.name)} /></td>
                                </tr>

                            ))
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default taskKinds