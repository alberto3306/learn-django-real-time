import React from 'react'
import Message from './Message'

const messageList = (props) => {

    const messages = props.messages.map((x) => {
        return <Message key={x} text={x} />
    })

    return (
        <div className="message-list">
            {messages}
        </div>
    )
}

export default messageList