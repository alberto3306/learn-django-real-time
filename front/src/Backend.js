import React, { Component } from 'react';
import Axios from './axios';
import Frontend from './Frontend';
import Socket from './Socket';


class Backend extends Component {

    state = {
        socket: null,
        connected: false,
        messages: [],
        kinds: [],
    }

    /* REACT METHODS */

    render() {

        const socketUrl = 'ws://' + this.props.host

        return (
            <>
                <Socket
                    onConnect={() => this.handleSocketConnect()}
                    onDisconnect={() => this.handleSocketDisconnect()}
                    onMessage={(message) => this.handleSocketMessage(message)}
                    url={socketUrl}
                />
                <Frontend
                    connected={this.state.connected}
                    messages={this.state.messages}
                    kinds={this.state.kinds}
                    createTaskCallback={(kind) => this.createTask(kind)}
                />
            </>
        )
    }

    componentDidMount() {
        this.fetchTaskKinds();
    }

    // COMPONENT METHODS

    fetchTaskKinds() {
        console.log('Fetching task kinds...')
        Axios.get('/kinds/', {}).then(response => this.setState({
            kinds: response.data,
        }))
    }

    createTask(kind) {
        console.log('Creating task...')
        Axios.post('/tasks/', { kind: kind }).then(response => console.log(response));
    }

    handleBackEvent(eventText) {

        const message = JSON.parse(eventText)

        switch (message.event) {
            case 'message':
                const newMessages = [...this.state.messages.slice(-9), message.text]
                this.setState({
                    messages: newMessages,
                })
                break

            case 'task_update':
                this.fetchTaskKinds()
                break

            default:
                console.log('Unknown message.')
                console.log(message)
                break
        }
    }

    // SOCKET HANDLERS

    handleSocketConnect() {
        this.setState({
            connected: true
        })
    }

    handleSocketDisconnect() {
        this.setState({
            connected: false
        })
    }

    handleSocketMessage(message) {
        this.handleBackEvent(message)
    }
}

export default Backend;