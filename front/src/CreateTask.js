import React from 'react'

const createTask = (props) => {

    const theClass = 'button button-default'

    return (
        <button type="button" className={theClass} onClick={props.callback}>Create task</button>
    )
}

export default createTask