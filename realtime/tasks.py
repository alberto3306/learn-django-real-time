import time
from random import randrange

from ._celery import app
from .consumers import send_message


@app.task(bind=True)
def task_tree(self, children=0, duration=10):
    send_message("New task %s %s %s" % (
        self.request.id,
        self.request.root_id,
        self.request.parent_id,
    ))
    for child in xrange(children):
        new_childs = randrange(children)
        new_duration = randrange(duration)
        send_message("Spawn child %s with %s childs and duration %s." % (
            child,
            new_childs,
            new_duration,
        ))
        task_tree.delay(new_childs, new_duration)

    for t in xrange(duration):
        send_message("Task %s time %s." % (self.request.id, t))
        time.sleep(1)

    send_message("Task %s done." % (self.request.id,))

    return children, duration
