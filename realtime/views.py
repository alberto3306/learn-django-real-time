import logging

from rest_framework import mixins
from rest_framework import viewsets

from django.template.response import TemplateResponse

from .models import Task, TaskKind
from .serializers import TaskSerializer, TaskKindSerializer
from .tasks import task_tree

logger = logging.getLogger(__name__)

class TaskViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):

    queryset = Task.objects.order_by('-pk')
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        kind = serializer.validated_data['kind']
        logger.info('Creating new task of kind "%s".', kind)
        Task.objects.run(kind=kind)


class TaskKindViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = TaskKind.objects.order_by('name')
    serializer_class = TaskKindSerializer
