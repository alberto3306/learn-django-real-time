import json

from channels import Group
from channels.handler import AsgiHandler
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http.response import HttpResponse

from .models import Task

GROUP = 'connections'


def send_message(text):
    return send_event('message', text=text)


def send_event(event, **kwargs):
    Group(GROUP).send({
        'text': json.dumps(dict(event=event, **kwargs)),
    })


def ws_connect(message):
    print 'connect'
    message.reply_channel.send({'accept': True})
    Group(GROUP).add(message.reply_channel)
    send_message('Client %s is watching.' % message.reply_channel)


def ws_receive(message):
    print 'receive:', message.content
    message.reply_channel.send({
        'text': message.content['text'],
    })


def ws_disconnect(message):
    print 'disconnect'
    Group(GROUP).discard(message.reply_channel)
    send_message('Client %s is no longer watching.' % message.reply_channel)


@receiver(post_save, sender = Task, dispatch_uid = 'post_save_task_send_event')
def post_save_task_send_event(sender, **kwargs):
    send_event('task_update')
