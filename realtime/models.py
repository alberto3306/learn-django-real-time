# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from django.db import IntegrityError, models, transaction
from django.utils import timezone


class TaskKind(models.Model):

    name = models.SlugField(max_length=36, unique=True)

    def __unicode__(self):
        return self.name


class TaskManager(models.Manager):

    @transaction.atomic
    def run(self, kind):

        new_uuid = unicode(uuid.uuid4())

        try:
            return RunningTask.objects.create(
                kind = kind,
                task = self.create(
                    kind=kind,
                    uuid=new_uuid,
                )
            )
        except IntegrityError as e:
            return False

    def running(self):
        return self.filter(running__isnull=False)


class Task(models.Model):

    kind = models.ForeignKey(TaskKind)
    uuid = models.CharField(max_length=36, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_completed = models.DateTimeField(null=True)
    success = models.BooleanField(default=False)

    objects = TaskManager()

    class Meta:
        default_related_name = 'tasks'

    def __unicode__(self):
        return self.uuid

    def done(self, success=False):
        self.running.delete()
        self.date_completed = timezone.now()
        self.success=success


class RunningTask(models.Model):

    task = models.OneToOneField(Task, related_name = 'running')
    kind = models.OneToOneField(TaskKind, unique=True, related_name = 'running')
    since = models.DateTimeField(auto_now_add=True)

    class Meta:
        default_related_name = 'running_tasks'
