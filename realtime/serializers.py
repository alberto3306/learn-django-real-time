from rest_framework import serializers

from .models import Task, TaskKind


class TaskSerializer(serializers.ModelSerializer):

    kind = serializers.SlugRelatedField(slug_field = 'name', queryset = TaskKind.objects.all())

    class Meta:
        model = Task
        fields = (
            'kind',
            'uuid',
            'date_created',
            'date_completed',
            'success',
        )
        read_only_fields = (
            'uuid',
            'date_created',
            'date_completed',
            'success',
        )

 

class TaskKindSerializer(serializers.ModelSerializer):

    running_since = serializers.DateTimeField(source = 'running.since')

    class Meta:
        model = TaskKind
        fields = (
            'name',
            'running_since',
        )
        read_only_fields = fields
